# How-To-Create-Coding-Story

## Introduction

## Demo

`explorer https://codingstories.io/become-author/guide`

`explorer https://github.com/NikiforovAll/codingstories-template`

`dotnet new story --no-devcontainer -n LiveDemo`

`cd LiveDemo`

`fd -H`

`rm -rf LiveDemo LiveDemo.Tests *.sln`

`dotnet new console`

`bat Program.cs`

`echo "System.Console.WriteLine("");" > Program.cs`

`bat Program.cs LiveDemo.csproj .story.md`

`git init`

`git add . && git commit -m "Initial Commit"`

`git remote add origin https://gitlab.com/NikiforovAll/live-coding-story-demo.git`

`git push --set-upstream origin main --force`

`explorer 'https://codingstories.io/story/https%3A%2F%2Fgitlab.com%2FNikiforovAll%2Flive-coding-story-demo'`

`git am ../hello-world.patch`

`bat Program.cs .story.md`

`git l`

`git push --force`

`explorer 'https://codingstories.io/story/https%3A%2F%2Fgitlab.com%2FNikiforovAll%2Flive-coding-story-demo'`

`git am ../wrap-up.patch`

`bat .story.md .story.json`

<!-- Before last commit consider adding changes to .story.md if have time and run `git commit --amend --no-edit" -->

`git l`

`git push --force`

`explorer 'https://codingstories.io/story/https%3A%2F%2Fgitlab.com%2FNikiforovAll%2Flive-coding-story-demo'`

## Useful links

1. Check coding story about coding story: <https://codingstories.io/story/https:%2F%2Fgitlab.com%2FNikiforovAll%2Fcoding-story-coding-story>
2. Check introduction blog post: <https://nikiforovall.github.io/dotnet/coding-stories/2021/03/14/coding-story.html>
3. Latest coding story: <https://codingstories.io/story/https:%2F%2Fgitlab.com%2FNikiforovAll%2Fhow-to-add-openapi-to-aspnetcore>
