# FizzBuzz

Return an array containing the numbers from 1 to N, where N is the parameterized value. `N` will never be less than 1.

Replace certain values however if any of the following conditions are met:

If the value is a multiple of 3: use the value "Fizz" instead
If the value is a multiple of 5: use the value "Buzz" instead
If the value is a multiple of 3 & 5: use the value "FizzBuzz" instead

```csharp
public static string[] GetFizzBuzzArray(int n)
```


## Hint - Story structure

You can write the story the way you want but for something like this I would recommend to have:

* Introduction to the problem
* Solution
* Test Cases Analysis (optional)
* Conclusion
