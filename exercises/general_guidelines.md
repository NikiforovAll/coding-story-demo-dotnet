# General Guidelines

1. Create `.story.md` file.

```text
---
focus: 
---

# CodingStoriesTemplate

## Outline

```

2. Initialize empty git repository `git init`.
3. Think about the structure and what ideas you try to convey.
4. Consult Author's guide <https://codingstories.io/become-author>.
5. Open on UI: <https://codingstories.io/become-author/guide#section11>
