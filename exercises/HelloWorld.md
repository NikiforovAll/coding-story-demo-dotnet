# Hello World

The classical introductory exercise. Just say "Hello, World!".

["Hello, World!"](http://en.wikipedia.org/wiki/%22Hello,_world!%22_program) is
the traditional first program for beginning programming in a new language
or environment.

- Write a function that returns the string "Hello, World!".

```csharp
public static string Hello()
```

## Hint - Story structure

You can write the story the way you want but for something like this I would recommend to have:

* Introduction to the problem
* Solution
* Conclusion

### Test Cases

```csharp
using Xunit;

public class HelloWorldTests
{
    [Fact]
    public void Say_hi_()
    {
        Assert.Equal("Hello, World!", HelloWorld.Hello());
    }
}
```
